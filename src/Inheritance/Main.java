package Inheritance;

import ProtectedTest.Student;

public class Main {
    public static void main(String[] args) {
//        Employee emp = new Employee("ali");
//        Manager mng = new Manager("mehdi");
//        emp.setName("ali");
//        System.out.println(emp.getName());
//        mng.setName("Mehdi");
//        System.out.println(mng.getName());
//        Employee emp1 = new Manager("reza");
//        System.out.println(emp1.getName());
        CEO ceo = new CEO("sadaf");
//        System.out.println(ceo.getName());
        Employee em = new Manager("hasan");
//        //polymorphism
//        emp.setSalary(1500);
//        mng.setSalary(1500);
//        System.out.println(emp.getSalary());
//        System.out.println(mng.getSalary());
//        //abstract
////        Person p = new Person();
//        float p = 3.14F;
//        int pi = (int) p;
        //instanceof
        if(em instanceof Employee)
        {
            System.out.println("em is employee");
        }
        if(em instanceof Manager)
        {
            System.out.println("em is manager");
        }
        if(em instanceof CEO)
        {
            System.out.println("em is ceo");
        }
        Employee emp2 = (Employee) em;
    }
}
