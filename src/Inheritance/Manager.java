package Inheritance;

public class Manager extends Employee{
    //field
    private int test;
    //constructor
    public Manager(String name)
    {
        super(name);
    }
    //this
    //super

    public float getSalary()
    {
        return (float) (super.getSalary()*1.5);
    }
}
