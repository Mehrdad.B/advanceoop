package Inheritance;

public class Employee extends Person{
    public Employee(String name)
    {
        super(name);
    }
    private float salary;

    @Override
    public void setIdentity() {

    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }
}
